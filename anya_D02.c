#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <libgen.h>
#include <ctype.h>
#include <dirent.h>

static const char *dirpath = "/home/shoy/Downloads";
static const int buffer_size = 1024;

char *get_directory(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    int n = strlen(path);
    int i = n - 1;
    while (i >= 0){
        if (path[i] == '/') break;
        i--;
    }
    i++;
    snprintf(filename, i, "%s", path);
    return filename;
}

char *get_filename(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    int n = strlen(path);
    int i = n - 1;
    while (i >= 0){
        if (path[i] == '/') break;
        i--;
    }
    i++;
    sprintf(filename, "%s", path + i);
    return filename;
}

char *get_filename_only(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    char *temppath = get_filename(path);
    int n = strlen(temppath);
    int i = 0;
    for (;i < n;i++){
        if (temppath[i] == '.'){
            break;
        }
    }
    snprintf(filename, i + 1, "%s", temppath);
    free(temppath);
    return filename;
}

char *get_filename_exetension(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    filename[0] = 0;

    if (!strstr(path, "."))
        return filename;
    
    char *temppath = get_filename(path);
    int n = strlen(temppath);
    int i = 0;
    for (;i < n;i++){
        if (temppath[i] == '.'){
            break;
        }
    }
    i++;

    if (strlen(temppath + i) != 0)
        sprintf(filename, "%s", temppath + i);
    
    free(temppath);
    return filename;
}

int change_to_upper(char *path){
    int val = 0;
    int n = strlen(path);
    int k = n;

    int i = 0;
    for (;i < k;i++){
        int diff = (path[i] == toupper(path[i]) ? 0 : 1);
        val <<= 1;
        val = val | diff;
    }

    return val;
}

char *change_to_special_directory(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    char *extension_only = get_filename_exetension(path);
    char *filename_only = get_filename_only(path);

    printf("%s\n", filename_only);
    printf("%s\n", extension_only);

    int diff = change_to_upper(filename_only);

    int i = 0;
    int n = strlen(filename_only);

    for (;i < n;i++)
        filename_only[i] = toupper(filename_only[i]);
    

    if (strlen(extension_only) == 0) sprintf(filename, "%s.%d", filename_only, diff);
    else 
        sprintf(filename, "%s.%s.%d", filename_only, extension_only, diff);

    free(filename_only);free(extension_only);

    return filename;
}

char *change_to_normal_directory(char *path){
    char *filename_only = get_filename_only(path);
    char *filename = malloc(sizeof(char) * buffer_size);
    char *extension_only_name = get_filename_only(get_filename_exetension(path));
    char *extension_only_number = get_filename_exetension(get_filename_exetension(path));

    int number_dot = 0;

    int i = 0, mask = 0, n = strlen(path);
    for (;i < n;i++) if (path[i] == '.') number_dot++;
    
    if (number_dot == 1) extension_only_number = extension_only_name;

    n = strlen(extension_only_number); i = 0;
    
    for (;i < n;i++){
        mask *= 10;
        mask += (extension_only_number[i] - '0');
    }

    i = 0; n = strlen(filename_only);
    for (;i < n;i++){
        if (mask & (1 << (n - i - 1)))
            filename[i] = toupper(filename_only[i]);
        else 
            filename[i] = filename_only[i];
        
    }

    filename[i] = '\0';

    char *ot = malloc(sizeof(char) * buffer_size);

    if (number_dot > 1)
        sprintf(ot, "%s.%s", filename, extension_only_name);
    else
        sprintf(ot, "%s", filename);

    return ot;
}

bool check_is_special_directory(char *path){
    return strstr(path, "nam_do-saq_") == path;
}

//mode << 0 == none,1 << 3 = special
int get_encryption_mode(char *path){
    if (strlen(path) == 0){
        return 0;
    }
    char *temp = path;

    int mode = 0;

    char *tok = strtok(temp, "/");
    char fpath[buffer_size];
    fpath[0] = 0;

    sprintf(fpath, "%s", dirpath);

    while (tok){
        sprintf(fpath + strlen(fpath), "/%s", tok);

        if (strstr(tok, "nam_do-saq_") == tok){
            mode = mode | 1 << 3;
            return mode;
        }
        tok = strtok(NULL, "/");
    }

    return mode;
}

bool is_decrypting = false;

char *get_encryption_path(const char * path){
    if (strlen(path) == 0 || path[0] == 0){
        return "/";
    }

    char *fpath = malloc(buffer_size);
    char temp[buffer_size];
    
    fpath[0] = 0;

    sprintf(temp, "%s", path);

    char *test = temp; char *mid = temp;

    int enc = 0;
    while (*mid != '\0'){
        int n = strlen(fpath);

        mid = test;
        while ((*mid) != '/' && (*mid) != '\0'){
            mid++;
        }

        char jawaban[buffer_size];
        snprintf(jawaban, mid - test + 1, "%s", test);

        if (enc == 0)
            sprintf(fpath + n, "/%s", jawaban);
        else if (enc & (1 << 3))
            sprintf(fpath + n, "/%s", change_to_special_directory(jawaban));
        

        int enc_temp = get_encryption_mode(jawaban);
        if (!(enc & (1 << 3))){
            enc = enc_temp;
        }
        test = mid + 1;
    }
    return fpath + 1;
}

char *get_decryption_path(const char * path){
    if (strlen(path) == 0 || path[0] == 0){
        return "/";
    }

    char *fpath = malloc(buffer_size);
    char temp[buffer_size];
    
    fpath[0] = 0;

    sprintf(temp, "%s", path);

    char *test = temp;
    char *mid = temp;

    int enc = 0;

    while (*mid != '\0'){
        int n = strlen(fpath);

        mid = test;
        while ((*mid) != '/' && (*mid) != '\0'){
            mid++;
        }

        char jawaban[buffer_size];
        snprintf(jawaban, mid - test + 1, "%s", test);

        if (enc == 0)
            sprintf(fpath + n, "/%s", jawaban);
        else if (enc & (1 << 3))
            sprintf(fpath + n, "/%s", change_to_normal_directory(jawaban));

        int enc_temp = get_encryption_mode(jawaban);
        if (!(enc & (1 << 3)))
            enc = enc_temp;
        

        test = mid + 1;
    }

    return fpath + 1;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    char fpath[buffer_size];
    sprintf(fpath, "%s%s", dirpath, get_decryption_path(path));

    printf("GetAttr : %s\n", fpath);

	int res;

	res = lstat(fpath, stbuf);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[buffer_size];
    char path_non_constant[buffer_size];

    sprintf(fpath, "%s%s", dirpath, get_decryption_path(path));
    sprintf(path_non_constant, "%s", path);

    printf("ReadDir : %s\n", fpath);

    int res = 0;

    int enc = get_encryption_mode(path_non_constant);

    DIR *dp;
    struct dirent *de;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (de->d_name[0] == '.') continue;
        struct stat st;

        char temp[buffer_size];

        if (enc == 0)
            sprintf(temp, "/%s", de->d_name);
        else if (enc & (1 << 3))
            sprintf(temp, "/%s", change_to_special_directory(de->d_name));
        

        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        printf("to be filler : %d -> %s\n", enc, de->d_name);
        printf("?? -> %s\n", temp);

        res = (filler(buf, temp + 1, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[buffer_size];
    sprintf(fpath, "%s%s", dirpath, get_decryption_path(path));

    printf("ReadFile : %s\n", fpath);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    char castpath[buffer_size];
    sprintf(castpath, "%s", to);
    
    char fpath[buffer_size];
    char tpath[buffer_size];
    sprintf(fpath, "%s%s", dirpath, get_decryption_path(from));
    sprintf(tpath, "%s%s/%s", dirpath, get_directory(get_decryption_path(castpath)), get_filename(castpath));

    printf("RenameFrom : %s\n", fpath);
    printf("RenameTo : %s\n", tpath);

	int res = rename(fpath, tpath); 
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_mkdir(const char * path, mode_t mode){
    char castpath[buffer_size];
    sprintf(castpath, "%s", path);
    
    char fpath[buffer_size];

    //sprintf(fpath, "%s%s", dirpath, get_decryption_path(path));
    sprintf(fpath, "%s%s/%s", dirpath, get_directory(get_decryption_path(castpath)), get_filename(castpath));

    printf("MkDir : %s\n", fpath);

    int res = mkdir(fpath, mode);

	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    char castpath[buffer_size];
    sprintf(castpath, "%s", path);
    
    char fpath[buffer_size];

    //sprintf(fpath, "%s%s", dirpath, get_decryption_path(path));
    sprintf(fpath, "%s%s/%s", dirpath, get_directory(get_decryption_path(castpath)), get_filename(castpath));

    printf("MakeNod : %s\n", fpath);

	int res;

	/* On Linux this could just be 'mknod(fpath, mode, rdev)' but this
	   is more portable */
	if (S_ISREG(mode)) {
		res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0)
			res = close(res);
	} else if (S_ISFIFO(mode))
		res = mkfifo(fpath, mode);
	else
		res = mknod(fpath, mode, rdev);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rmdir(const char *path){
    char fpath[buffer_size];
    sprintf(fpath, "%s%s", dirpath, get_decryption_path(path));

    printf("RmDir : %s\n", fpath);
    
    int res = rmdir(fpath);
    if(res == -1){
        return -errno;
    }

    return 0;
}

static int xmp_unlink(const char *path)
{
    char fpath[buffer_size];
    sprintf(fpath, "%s%s", dirpath, get_decryption_path(path));
    
    printf("Unlink : %s\n", fpath);

	int res;

	res = unlink(fpath);
	if (res == -1)
		return -errno;

	return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .rename = xmp_rename,   
    .read = xmp_read,
    .mknod = xmp_mknod
};

int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}