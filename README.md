# soal-shift-sisop-modul-4-D02-2022

## Anggota Kelompok ##

NRP | Nama
---------- | ----------
5025201028 | Muhammad Abror Al Qushoyyi
5025201221 | Naufal Faadhilah
05111940000211 | Vicky Thirdian

## Soal anya_D02
Pada soal no1 bagian a, diperintahkan untuk semua direktori dengan awalan "Animeku_" terencode dengan ketentuan file yang terdapat huruf besar akan terencode dengan atbash cipher dan huruf kecil terencode dengan rot13. Berikut merupakan funsi dari encodenya
```
void encode(char *str)
{
   int i = 0;
   while (str[i] != '\0' && str[i] != '.')
   {
       // atbash cipher
       if (str[i] >= 'A' && str[i] <= 'Z')
       {
           str[i] =  'Z' + 'A' - str[i];
       }
       // rot13
       else if (str[i] >= 'a' && str[i] <= 'z')
       {
           str[i] = ((str[i] - 97) + 13) % 26 + 97;
       }
 
       i++;
   } 
}
```

Pada soal bagian b, diperintahkan untuk direname dengan awalan "Animeku_" yang mana direktoru tersebut akan menjadi terencode dengan ketentuan yang dengan dengan soal bagian a. Sebelumnya program tersebut harus membaca directory tersebut dari fungsi xmp_readdir
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
   int res = 0;
 
   DIR *dp;
   struct dirent *de;
   (void)offset;
   (void)fi;
 
   dp = opendir(get_real_path(path));
 
   if (dp == NULL)
       return -errno;
 
   while ((de = readdir(dp)) != NULL)
   {
       struct stat st;
 
       memset(&st, 0, sizeof(st));
 
       st.st_ino = de->d_ino;
       st.st_mode = de->d_type << 12;
       char name[100] ;
       strcpy(name, de->d_name);
       if (strstr(path, "/Animeku_"))
       {
           encode(name);
       }
       res = (filler(buf, name, &st, 0));
 
       if (res != 0)
           break;
   }
 
   closedir(dp);
 
   _log(leveli, "CD", path);
 
   return 0;
}
```
Setelah itu kita perlu membaca file tersebut sebelum bisa melakukan rename dengan menggunakan fungsi xmp_read 
```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
   printf("xmp_read\n");
   char fpath[1000];
 
   strcpy(fpath, get_real_path(path));
 
   int res = 0;
   int fd = 0;
 
   (void)fi;
 
   fd = open(fpath, O_RDONLY);
 
   if (fd == -1)
       return -errno;
 
   res = pread(fd, buf, size, offset);
 
   if (res == -1)
       res = -errno;
 
   close(fd);
 
   //bikin lognya
   _log(leveli, "READ", fpath);
 
   return res;
}
```
Lalu untuk melakukan rename kita menggunakan fungsi xmp_getattr
```
static int xmp_getattr(const char *path, struct stat *stbuf)
{
   int res;
   char fpath[1000];
 
   strcpy(fpath, get_real_path(path));
 
   res = lstat(fpath, stbuf);
 
   if (res == -1)
       return -errno;
 
   _log(leveli, "LS", fpath);
 
   return 0;
}
```
Lalu untuk setiap data yang terencode akan masuk ke file Wibu.log. Berikut merupakan codenya.
```
void write_log(char oldname[], char newname[])
{
   FILE *fp = fopen(LOG, "a+");
}
```
Agar sesuai dengan perintahnya seperti ```xyzpath --> zyxpath``` maka digunakan
```
   fprintf(fp, "%s --> %s\n", oldname, newname);

```

Lalu untuk bagian e. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya. Maka digunakan fungsi xmp_readdir untuk melakukan melakukan pengecekan. 
```
 while ((de = readdir(dp)) != NULL)
   {
       struct stat st;
 
       memset(&st, 0, sizeof(st));
 
       st.st_ino = de->d_ino;
       st.st_mode = de->d_type << 12;
       char name[100] ;
       strcpy(name, de->d_name);
       if (strstr(path, "/Animeku_"))
       {
           enc(name);
       }
       res = (filler(buf, name, &st, 0));
 
       if (res != 0)
           break;
   }
```


soal yang terakhir ini diminta untuk merubah directory normal menjadi directory special, dengan menambahkan format nama didepannya `nam_do-saq`. 

insialisasi path yang akan di mount ke directory lain

```C
static const char *dirpath = "/home/shoy/Downloads";
```

fungsi untuk mendapatkan directory, nama file, nama file tanpa extensionnya, dan extension tanpa nama filenya

```C
char *get_directory(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    int n = strlen(path);
    int i = n - 1;
    while (i >= 0){
        if (path[i] == '/') break;
        i--;
    }
    i++;
    snprintf(filename, i, "%s", path);
    return filename;
}

char *get_filename(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    int n = strlen(path);
    int i = n - 1;
    while (i >= 0){
        if (path[i] == '/') break;
        i--;
    }
    i++;
    sprintf(filename, "%s", path + i);
    return filename;
}

char *get_filename_only(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    char *temppath = get_filename(path);
    int n = strlen(temppath);
    int i = 0;
    for (;i < n;i++){
        if (temppath[i] == '.'){
            break;
        }
    }
    snprintf(filename, i + 1, "%s", temppath);
    free(temppath);
    return filename;
}

char *get_filename_exetension(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    filename[0] = 0;

    if (!strstr(path, "."))
        return filename;
    
    char *temppath = get_filename(path);
    int n = strlen(temppath);
    int i = 0;
    for (;i < n;i++){
        if (temppath[i] == '.'){
            break;
        }
    }
    i++;

    if (strlen(temppath + i) != 0)
        sprintf(filename, "%s", temppath + i);
    
    free(temppath);
    return filename;
}
```

kemudian dibawah ini merupakan fungsi untuk mengubah nama filenya menjadi `UPPERCASE` saat berada di dalam fuse

```C
int change_to_upper(char *path){
    int val = 0;
    int n = strlen(path);
    int k = n;

    int i = 0;
    for (;i < k;i++){
        int diff = (path[i] == toupper(path[i]) ? 0 : 1);
        val <<= 1;
        val = val | diff;
    }

    return val;
}
```

 mengetahui path tersebut adalah dengan penentuna mode. Jadi penentuan mode adalah ketika suatu path dipanggil dengan mode `get_encryption_mode` menghasilkan `1 << 3` maka folder tersebut merupakan directory special. 

```C
int get_encryption_mode(char *path){
    if (strlen(path) == 0){
        return 0;
    }
    char *temp = path;

    int mode = 0;

    char *tok = strtok(temp, "/");
    char fpath[buffer_size];
    fpath[0] = 0;

    sprintf(fpath, "%s", dirpath);

    while (tok){
        sprintf(fpath + strlen(fpath), "/%s", tok);

        if (strstr(tok, "nam_do-saq_") == tok){
            mode = mode | 1 << 3;
            return mode;
        }
        tok = strtok(NULL, "/");
    }

    return mode;
}
```
Setiap folder dan files yang berada pada directory special akan di decode untuk merubah directory normal menjadi directory special.

misalnya jika pada file asli didalam directory `nam_do_saq` namanya adalah `isHaQ_KEreN.txt` maka pada fuse akan menjadi `ISHAQ_KEREN.txt.1670`. 1670 berasal dari biner 11010000110

dimana semua yang uppercase dimask 1, dan selain itu dimask 0 sehingga menghasilkan extension hasil dari biner. Dengan menggunakan fungsi di bawah

```C
char *change_to_special_directory(char *path){
    char *filename = malloc(sizeof(char) * buffer_size);
    char *extension_only = get_filename_exetension(path);
    char *filename_only = get_filename_only(path);

    printf("%s\n", filename_only);
    printf("%s\n", extension_only);

    int diff = change_to_upper(filename_only);

    int i = 0;
    int n = strlen(filename_only);

    for (;i < n;i++)
        filename_only[i] = toupper(filename_only[i]);
    

    if (strlen(extension_only) == 0) sprintf(filename, "%s.%d", filename_only, diff);
    else 
        sprintf(filename, "%s.%s.%d", filename_only, extension_only, diff);

    free(filename_only);free(extension_only);

    return filename;
}
```

dan ini untk mengembalikan ke directory normalnya

```C
char *change_to_normal_directory(char *path){
    char *filename_only = get_filename_only(path);
    char *filename = malloc(sizeof(char) * buffer_size);
    char *extension_only_name = get_filename_only(get_filename_exetension(path));
    char *extension_only_number = get_filename_exetension(get_filename_exetension(path));

    int number_dot = 0;

    int i = 0, mask = 0, n = strlen(path);
    for (;i < n;i++) if (path[i] == '.') number_dot++;
    
    if (number_dot == 1) extension_only_number = extension_only_name;

    n = strlen(extension_only_number); i = 0;
    
    for (;i < n;i++){
        mask *= 10;
        mask += (extension_only_number[i] - '0');
    }

    i = 0; n = strlen(filename_only);
    for (;i < n;i++){
        if (mask & (1 << (n - i - 1)))
            filename[i] = toupper(filename_only[i]);
        else 
            filename[i] = filename_only[i];
        
    }

    filename[i] = '\0';

    char *ot = malloc(sizeof(char) * buffer_size);

    if (number_dot > 1)
        sprintf(ot, "%s.%s", filename, extension_only_name);
    else
        sprintf(ot, "%s", filename);

    return ot;
}
```

kemudian operation fuse yang digunakan antara lain

```C
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .rename = xmp_rename,   
    .read = xmp_read,
    .mknod = xmp_mknod
};
```

> hasil

saat program dijalankan

![image](https://media.discordapp.net/attachments/872527165240004652/975392726608670881/unknown.png)

menambahkan format `nam_do-saq`

![image](https://media.discordapp.net/attachments/872527165240004652/975392726986162236/unknown.png)

tanpa format `nam_do-saq`

![image](https://media.discordapp.net/attachments/872527165240004652/975392727711752292/unknown.png)



